$(document).ready(function() {
  initSlider();
  $('.header .fixed').hide();

  $(window).scroll(function(){
    if ($(window).scrollTop() >= 180) {
      $('.header .fixed').slideDown(250);
    } else {
      $('.header .fixed').slideUp(100);
    }
  });

  setTimeout(function() {
    $.each($("img"), function() {if ($(this).width() > 250) { $(this).addClass("img-responsive")}});
  }, 500);
});

function initSlider() {
  $.each($('#background-images ul li'), function(index, item) {
    var first_or_not = "";
    if (index === 0) {
      first_or_not = "active";
    }

    $('.carousel-indicators').append('<li class="' + first_or_not + '" data-target="#slider" data-slide-to="' + index + '"></li>');
    $('.carousel-inner').append($('<div style="background-image: url(\'' + $(item).data('large-url') + '\')" class="item ' + first_or_not + '"></div>').append('<div class="carousel-caption"><span>' + $(item).data('title') + '</span></div>'));
  });

  $('#slider').carousel();
}
