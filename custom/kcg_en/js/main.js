function has_images(){
  return $('#page-images ul[data-type="page_images"] li').size() > 0;
}

function create_carousel(){
  var container = $('#page-heading-container');
  // Add 'slideshow' class to make it bigger
  if(small_display()){
    container.addClass('slideshow-small');
  }else{
    container.addClass('slideshow');
  }
  ////// carousel code ///////
  var carousel_id = 'carousel-heading';
  var carousel = '<div id="' + carousel_id + '" class="carousel slide" data-ride="carousel">';
  carousel += '<div class="carousel-inner">';
  carousel += images_for_slider('#page-images li'); // Add img tags to carousel
  carousel += '</div>';
  carousel += '<a class="left carousel-control" href="#' + carousel_id + '" data-slide="prev">';
  carousel += '<span class="arrow-left"></span>';
  carousel += '</a>';
  carousel += '<a class="right carousel-control" href="#' + carousel_id + '" data-slide="next">';
  carousel += '<span class="arrow-right"></span>';
  carousel += '</a>';
  carousel += '</div>';

  // Add carousel to the page
  container.append(carousel);
  return carousel_id;
}

function images_for_slider(selector){
  var container_height = $('#page-heading-container').height();
  var image_items = '';

  // Generate image tags
  $.each($(selector), function(index, item){
    var img_url = $(item).data('url');
    var img_title = $(item).data('title');
    var img_description = $(item).data('description');
    var active = '';

    if(index == 0){
      active = ' active'
    }

    image_items += '<div class="item'+ active + '">';
    image_items += '<div style="';
    image_items += 'background-image:url(\'' + img_url +'\');';
    image_items += 'background-position: center;';
    image_items += 'background-repeat: no-repeat;';
    image_items += 'background-size: cover;';
    image_items += 'height: '+ container_height +'px;"></div>';

    image_items += '<div class="carousel-caption">';
    image_items += '<p>' + img_description + '</p>';

    image_items += '</div></div>'; // close '.item' and carousel caption divs
  });
  return image_items;
}

// Convert to five columns layout
function to_five_col(){
  $('.page-content').parent().addClass('five-columns');
  $('.procedures-box').parent().appendTo($('#page-body-container > .wrapper > .row').first());
}

// Manage accordions when "collapse" class is present
function toggle_collapse(){
  $('.page-heading').on('click', function(){
    $('.collapse.in').collapse('hide');
    collapsable = $(this).parents('.row').next().find('.collapse');
    collapsable.collapse('show');
  });
}

function add_language_flag(){
  var li_flag = '<li class="list-item">' +
                '<a href="http://www.kcg.lu">' +
                '<span class="flag-fr">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>' +
                '</a></li>';
  $('ul.menubar.menubar-head').append(li_flag);
}

function mobile_display(){
  if($(document).width() < '768')
    return true;
}

function small_display(){
  var document_size = $(document).width();
  if(document_size >= '768' && document_size <= '992')
    return true;
}

// Init
$(document).ready(function(){
  // Detect the user agent
  var doc = document.documentElement;
  doc.setAttribute('data-useragent', navigator.userAgent);

  if($('body').hasClass('home')){
    // Add gray background
    $('#page-body-container').addClass('content-background-color');
    // Add link to "nos services" page
    $.each($('.page-content'), function(index, item){
      var css_name = $(item).attr('class').split(' ')[1];
      css_name = css_name.replace('-box', '');
      $(item).on('click', function(){
        window.open('our-services.html#' + css_name + '-anchor', '_self')
      });
    });
    // Convert to five columns layout
    to_five_col();

    if(mobile_display())
      $('.page-heading-title').removeClass('hidden');
  }else{
    $('.footer-breadcrumbs').removeClass('hidden');
    $('.page-heading-title').removeClass('hidden');
    toggle_collapse();
  }

  if(has_images() && !mobile_display()){
    var carousel_id = create_carousel();
    $('#' + carousel_id).carousel();
  }

  add_language_flag();
});
