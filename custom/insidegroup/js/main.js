var image_data = []; // Array containing the list of background image's data
var BREAKPOINT = 768;

$(document).ready(function(){
  $("#slides").hide();

  set_news_list();

  $(".navigation > .menubar-side > .list-item:not(:first)").on("click", function(e) {
    e.preventDefault();

    $('.arrow-news').addClass('hidden');
    $(".navigation > .menubar-side > .list-item ul").hide();
    $(".navigation > .menubar-side > .list-item").not(this).animate({
      'height': '85px'
    });

    $(this).animate({
      'height': '340px'
    });
    $(this).find("ul").each(function() {
      $(this).show();
    });

    if ($(this).data('title-slug') == 'news') {
      $('.arrow-news').removeClass('hidden');
    } else {
      $('.arrow-news').addClass('hidden');
    }
  });

  $(".navigation > .menubar-side > .list-item ul li a").on("click", function() {
    document.location = $(this).attr("href");
  });

  var list_items = $('#background-images li');

  // Load mobile css
  sizeit_init();

  try {
    for (var i = 0; i < 3; i++) {
      // Collect image's data
      $.each(list_items, function(index, item) {
        // Create the Array with images' data
        image_data.push({
          page_id: $(item).data('page-id'),
          url: $(item).data('huge-url'),
          medium_url: $(item).data('medium-url'),
          image_title: $(item).data('title'),
          image_description: $(item).data('description')
        });
      });
    }
  } catch(err) {}

  $(".navigation > .menubar-side > .list-item.active ul.dropdown-menu").show();

  $("body").keydown(function(e) {
    if(e.which == 37) {
      $('.arrow.left').trigger("click");
    }
    else if(e.which == 39) {
      $('.arrow.right').trigger("click");
    }
  });

  setTimeout(function() {
    $("#slides").fadeIn(1000);
  }, 1000);

  // Show contend box if a content is present.
  show_content_box('.box-content');

  // Bind click event to switch the images
  if(image_data.length > 1){
    $('.arrow').removeClass('hidden');
    $('.arrow').on('click', move_slider_image);
  }

  // Open side menu
  var selected_menu_block = toggle_menu_animation();

  if (is_small_display())
    move_top_navigation();

  // Add icons on side-bar menu
  sidebar_menu_images();

  // Set the slideshow on desktop version only
  if (is_small_display()){
    image_for_mobile(selected_menu_block);
    inject_content_for_mobile(selected_menu_block);
  }else{
    // Set description of first image
    try {
      if( image_data[0]['image_title'] != "" ){
        set_image_details(
          image_data[0]['page_id'],
          image_data[0]['image_title'],
          image_data[0]['image_description']
        )
      }

      images_for_slideshow('#slides');
    } catch(err) {}
  }

  open_news_box();
  add_fb_link();
});

function is_small_display(){
  return (sizeit.width() <= BREAKPOINT);
}

function image_for_mobile(selected_menu_block){
  $(selected_menu_block).after(
    '<div class="img-mini" style="background-image:url(\'' + image_data[0]['medium_url'] +'\');"></div>'
  );
}

function inject_content_for_mobile(selected_menu_block){
  if(selected_menu_block && selected_menu_block.children().hasClass('news')){
    $('.img-mini').remove();
    $(selected_menu_block).after($('<li>').append($('.page-box-container')));
    $('.box-content-news').css('text-transform', 'none');
    $('.box-content-news').removeClass('box-content-news');
  }else{
    $('li.list-item.active').after($('<li>').append($('.page-box-container')));
  }
}

function show_content_box(selector){
  var container = $(selector);
  if (container.text().trim() != ''){
    container.removeClass('hidden');
  }
}

function set_image_details(page_id, image_title, image_description){
  var div = '<div class="float-right image-counter">/</div><div class="image-details clear"><p>';
  div += '<span class="uppercase">' + image_title + '</span><br/>';
  div += '<span>' + image_description + '</span>';
  div += '</p></div>';
  $('li[data-page-id="' + page_id + '"] > .image-details').remove();
  $('li[data-page-id="' + page_id + '"] > .image-counter').remove();
  $('li[data-page-id="' + page_id + '"]').append(div);
}

function toggle_menu_animation(){
  var selected_menu_block = null;
  var dropdown_menu_height = '340px';
  if (is_small_display())
    dropdown_menu_height = '240px';

  if( $('.dropdown.active').size() > 0 ){
    $('.dropdown.active').css('height', dropdown_menu_height);
    $('.dropdown.active > ul.dropdown-menu').show();
    selected_menu_block = $('.dropdown.active');
  }else if( $('.dropdown-menu li.active').size() > 0 ){
    var parent = $('li.active').parents('ul');
    parent.show();
    parent.parents('li').css('height', dropdown_menu_height);
    selected_menu_block = parent.parent();
  }

  return selected_menu_block;
}

function move_top_navigation(){
  var navigation_top = $('.navigation.top');
  var menubar = $('ul.menubar-head');
  navigation_top.appendTo('.left-column');
  navigation_top.removeClass('top').addClass('side');
  menubar.removeClass('menubar-head').addClass('menubar-side');
}

function open_news_box(){
  $('.arrow-news').on('click', function(e){ vertical_scroll(e); });
  $("li.list-item[data-title-slug=news] ul.dropdown-menu li").appendTo(".news-list-wrapper ul.dropdown-menu");

  var news_box = $('.menubar-side li[data-title-slug="news"]');
  if( ($('.navigation.top li.active').size() > 0) || news_box.is('.active') || (news_box.find('li.active').size() > 0) ){
    news_box.css('height', '340px');
    $('.arrow-news').removeClass('hidden');

    $.each(news_box.find('li'), function(index, item){
      if( $(item).is('.active') ){
        vertical_scroll(index);
      }
    });

    $(".news-list-wrapper ul.dropdown-menu").show();
  }

}

function sidebar_menu_images(){
  $.each($('.menubar-side > li'), function(index, item){
    var slug = $(item).data('title-slug');
    var img_tag = '<img src="' + STATIC_URL + '/img/' + slug + '.png" width=230 height=85 />';
    $(item).children('a').first().html(img_tag);
  });
}

function images_for_slideshow(selector){
  $(selector).html('');
  var images_count = image_data.length;
  var image_width = (100 / images_count).toString() + '%';
  var slider_width = (images_count * 100).toString() + '%';

  $.each(image_data, function(index, item){
    $(selector).append('<div style="background-image:url(\'' + item['url'] +'\'); width: ' + image_width + '"></div>');
  });
  $(selector).css({
    'width': slider_width,
    'left': "0%"
  });

  show_image_details(0);
}

function move_slider_image(e){
  var direction = 1; // left
  var images_count = image_data.length;
  var current_position = parseInt($('#slides')[0].style.left);
  var position_index = (Math.abs(current_position / 100));

  try {
    if( $(e.currentTarget).is('.right') ){
      direction = -1; // right
    }
  } catch(error) {
    direction = -1;
  }

  var new_position = current_position + (direction * 100);

  if ((direction == -1) && (position_index == (images_count - 1))) {
    new_position = 0;
  } else if ((direction == 1) && (current_position / 100) == 0) {
    new_position = (images_count - 1) * 100 * -1;
  }

  var new_position_index = Math.abs(new_position / 100);

  $('.arrow').unbind('click')

  $('#slides').animate({
    'left': (new_position).toString() + '%',
  }, 500, function() {
    $('.arrow').on('click', move_slider_image);
    show_image_details(new_position_index);
  });
}

function show_image_details(position_index){
  data = image_data[position_index];
  if( data['image_title'] != "" ){
    set_image_details(data['page_id'], data['image_title'], data['image_description']);
  }
  position_index_string = (position_index + 1).toString();
  if (position_index_string > ((image_data.length / 3) * 2)) {
    position_index_string = position_index_string - ((image_data.length / 3) * 2);
  }
  if (position_index_string > image_data.length / 3) {
    position_index_string = position_index_string - (image_data.length / 3);
  }

  $('.image-counter').text(position_index_string + '/' + (image_data.length / 3).toString());
}

function sizeit_init(){
  sizeit.configure(
  {
    max: BREAKPOINT,
    css: STATIC_URL + '/css/xs.css',
    name: 'extra-small'
  },
  {
    css: STATIC_URL + '/css/empty.css'
  });
}

function auto_start_slider() {
  $('.arrow').hide();
  setInterval(function() {
    move_slider_image();
  }, 2500);
}

function set_news_list(){
  var news_links = $('#news-list .page-news-list');
  var news_box = $('.menubar-side li[data-title-slug="news"]');
  var news_list_container = $('<div class="news-list-container">');
  var news_list_wrapper = $('<div class="news-list-wrapper">');
  var news_list = $('<ul class="dropdown-menu">');

  news_box.addClass('dropdown');
  if($('body').is('.inside') || $('body').is('.about-us') || $('body').is('.our-brands') || $('body').is('.contact'))
    news_box.addClass('active');

  $.each(news_links, function(index, item){
    if($(item).is('.active')){
      var active = ' class="active"';
    }else{
      var active = '';
    }
    news_list.append($('<li' + active + '>' + '<a href="' + $(item).children('a').attr('href') + '">' + $(item).text() + '</a></li>'));
  });

  // Add to main container
  news_list_wrapper.append(news_list);
  news_list_container.append(news_list_wrapper);
  news_box.append($('<div class="arrow-news up hidden">'));
  news_box.append(news_list_container);
  news_box.append($('<div class="arrow-news down hidden">'));
}

function add_fb_link(){
  var li = '<li class="list-item facebook" data-title-slug="facebook">' +
           '<a href="https://www.facebook.com/INSIDE.LU"><img src="http://insidegroup.lu/statics/img/facebook.png"></a>' +
           '</li>';
  $('.menubar.menubar-head').append(li);
}

function vertical_scroll(e){
  var container = $('.news-list-container');
  var wrapper = $('.news-list-wrapper');

  // "e" could be ether the "click" event or the position index
  if(typeof e == "number"){
    move_factor = parseInt(wrapper.height() / wrapper.find('li').size());
    container.scrollTo(move_factor * e);
  }else{
    e.stopPropagation();

    if($(e.target).is('.up')){
      container.scrollTo(container.scrollTop() - 175);
    }else{
      container.scrollTo(container.scrollTop() + 175);
    }
  }
}

$.fn.scrollTo = function( target, options, callback ){
  if(typeof options == 'function' && arguments.length == 2){ callback = options; options = target; }
  var settings = $.extend({
    scrollTarget  : target,
    offsetTop     : 50,
    duration      : 500,
    easing        : 'swing'
  }, options);
  return this.each(function(){
    var scrollPane = $(this);
    var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
    var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
    scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
      if (typeof callback == 'function') { callback.call(this); }
    });
  });
}
